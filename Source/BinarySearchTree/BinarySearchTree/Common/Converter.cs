﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinarySearchTree.Iterator;

namespace BinarySearchTree.Common
{
	public static class Converter<T>
	{
		public static string ToString(IIteratorImp<T> iteratorImp)
		{
			string result = "";
			IIterator<T> i = iteratorImp.GetIterator();

			for (i.First(); !i.IsDone(); i.Next())
				result += i.Current() + "\t";
			return result;
		}
	}
}
