﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinarySearchTree.Models;

namespace BinarySearchTree.Criteria
{
	public interface ICriteria<T>
	{
		bool Check(BinaryTreeNode<T> node);
	}
}
