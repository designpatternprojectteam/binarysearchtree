﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinarySearchTree.Models;

namespace BinarySearchTree.Criteria
{
	public class IsLeafCriteria<T> : ICriteria<T>
	{
		public bool Check(BinaryTreeNode<T> node)
		{
			if (node.IsLeaf())
				return true;
			return false;
		}
	}
}
