﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BinarySearchTree.Common;
using BinarySearchTree.Comparer;
using BinarySearchTree.Criteria;
using BinarySearchTree.Iterator;
using BinarySearchTree.Models;

namespace BinarySearchTree
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
			var bst = new BinarySearchTree<int>(new ArrayIntegerImp(
													new List<int>() { 6, 8, 4, 3, 10 }
												)
											, new IntegerAscendingComparer());
			textBox1.Text = Converter<int>.ToString(bst.InorderTraversal());
	        textBox2.Text = bst.Count(new IsLeafCriteria<int>()).ToString();
        }
    }
}
