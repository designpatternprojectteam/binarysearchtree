﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Iterator
{
	public class ArrayIntegerBackwardIterator : IIterator<int>
	{
		private List<int> _data;
		private int _currentPos;

		public ArrayIntegerBackwardIterator(List<int> data)
		{
			_data = data;
			First();
		}
		public void First()
		{
			_currentPos = _data.Count - 1;
		}

		public void Next()
		{
			if (IsDone())
				return;

			_currentPos--;
		}

		public bool IsDone()
		{
			return _currentPos < 0;
		}

		public int Current()
		{
			return IsDone() ? -1 : _data[_currentPos];
		}
	}
}
