﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Iterator
{
	public class ArrayIntegerForwardIterator : IIterator<int>
	{
		private List<int> _data;
		private int _currentPos;

		public ArrayIntegerForwardIterator(List<int> data)
		{
			_data = data;
		}
		public void First()
		{
			_currentPos = 0;
		}

		public void Next()
		{
			if (IsDone())
				return;

			_currentPos++;
		}

		public bool IsDone()
		{
			return _currentPos == _data.Count;
		}

		public int Current()
		{
			return IsDone() ? -1 : _data[_currentPos];
		}
	}
}
