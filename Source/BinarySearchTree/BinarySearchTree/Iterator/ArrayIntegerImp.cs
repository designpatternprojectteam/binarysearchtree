﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Iterator
{
	public class ArrayIntegerImp : IIteratorImp<int>
	{
		private List<int> _data;

		public ArrayIntegerImp()
		{
			_data = new List<int>();
		}
		public ArrayIntegerImp(List<int> data)
		{
			_data = data;	
		}
		public void Add(int value)
		{
			_data.Add(value);
		}

		public IIterator<int> GetIterator()
		{
			return new ArrayIntegerForwardIterator(_data);
		}

		public IIteratorImp<int> CloneNew()
		{
			return new ArrayIntegerImp();
		}
	}
}
