﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Iterator
{
	public interface IIteratorImp<T>
	{
		void Add(T data);
		IIterator<T> GetIterator();
		IIteratorImp<T> CloneNew();
	}
}
