﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BinarySearchTree.Comparer;
using BinarySearchTree.Criteria;
using BinarySearchTree.Iterator;

namespace BinarySearchTree.Models
{
	class BinarySearchTree<T> : BinaryTree<T>
	{
		private Comparer<T> _comparer;
		private IIteratorImp<T> _iteratorImp;
		public BinarySearchTree(IIteratorImp<T> iteratorImp, Comparer<T> comparer)
		{
			_comparer = comparer;
			_iteratorImp = iteratorImp;
			Import(iteratorImp);
		}

		public void Import(IIteratorImp<T> iteratorImp)
		{
			Clear();

			IIterator<T> i = iteratorImp.GetIterator();

			for (i.First(); !i.IsDone(); i.Next())
				Insert(new BinaryTreeNode<T>(i.Current()));
		}
		public void Insert(BinaryTreeNode<T> node)
		{
			if (Root == null)
			{
				Root = new BinaryTreeNode<T>();
			}
			Insert(Root, node);
		}
		public void Insert(BinaryTreeNode<T> parent, BinaryTreeNode<T> child)
		{
			if (parent.IsEmpty())
			{
				parent.Import(child);
				return;
			}
			if (_comparer.Compare(parent.Data, child.Data) > 0)
			{
				if (parent.Left == null)
				{
					parent.Left = new BinaryTreeNode<T>();
				}
				Insert(parent.Left, child);
			}
			else
			{
				if (parent.Right == null)
				{
					parent.Right = new BinaryTreeNode<T>();
				}
				Insert(parent.Right, child);
			}
		}

		/// <summary>
		/// Left - Node - Right
		/// </summary>
		public IIteratorImp<T> InorderTraversal()
		{
			IIteratorImp<T> inorderIterator = _iteratorImp.CloneNew();
			InorderTraversal(Root, inorderIterator);
			return inorderIterator;
		}
		public void InorderTraversal(BinaryTreeNode<T> node, IIteratorImp<T> iteratorImp)
		{
			if (node == null)
				return;
			InorderTraversal(node.Left, iteratorImp);
			iteratorImp.Add(node.Data);
			InorderTraversal(node.Right, iteratorImp);
		}

		/// <summary>
		/// Node - Left - Right
		/// </summary>
		public IIteratorImp<T> PreorderTraversal()
		{
			IIteratorImp<T> preorderIterator = _iteratorImp.CloneNew();
			PreorderTraversal(Root, preorderIterator);
			return preorderIterator;
		}
		public void PreorderTraversal(BinaryTreeNode<T> node, IIteratorImp<T> iteratorImp)
		{
			if (node == null)
				return;
			iteratorImp.Add(node.Data);
			PreorderTraversal(node.Left, iteratorImp);
			PreorderTraversal(node.Right, iteratorImp);
		}

		/// <summary>
		/// Left - Right - Node
		/// </summary>
		public IIteratorImp<T> PostorderTraversal()
		{
			IIteratorImp<T> postorderIterator = _iteratorImp.CloneNew();
			PostorderTraversal(Root, postorderIterator);
			return postorderIterator;
		}
		public void PostorderTraversal(BinaryTreeNode<T> node, IIteratorImp<T> iteratorImp)
		{
			if (node == null)
				return;
			PostorderTraversal(node.Left, iteratorImp);
			PostorderTraversal(node.Right, iteratorImp);
			iteratorImp.Add(node.Data);
		}

		public int Count(ICriteria<T> criteria)
		{
			int count = 0;
			Count(Root, criteria, ref count);
			return count;
		}
		public void Count(BinaryTreeNode<T> node, ICriteria<T> criteria, ref int count)
		{
			if (node == null)
				return;
			if (criteria.Check(node))
			{
				count++;
			}
			Count(node.Left, criteria, ref count);
			Count(node.Right, criteria, ref count);
		}
	}
}
