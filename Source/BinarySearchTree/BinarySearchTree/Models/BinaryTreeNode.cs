﻿namespace BinarySearchTree.Models
{
	public class BinaryTreeNode<T> : Node<T>
	{
		public BinaryTreeNode() : base() { }
		public BinaryTreeNode(T data) : base(data, null) { }

		public BinaryTreeNode(BinaryTreeNode<T> node)
		{
			Import(node);
		}
		public BinaryTreeNode(T data, BinaryTreeNode<T> left, BinaryTreeNode<T> right)
		{
			Data = data;
			NodeList<T> children = new NodeList<T>(2);
			children[0] = left;
			children[1] = right;

			Childs = children;
		}

		public BinaryTreeNode<T> Left
		{
			get
			{
				if (Childs == null)
					return null;
				return (BinaryTreeNode<T>)Childs[0];
			}
			set
			{
				if (Childs == null)
					Childs = new NodeList<T>(2);

				Childs[0] = value;
			}
		}

		public BinaryTreeNode<T> Right
		{
			get
			{
				if (Childs == null)
					return null;
				return (BinaryTreeNode<T>)Childs[1];
			}
			set
			{
				if (Childs == null)
					Childs = new NodeList<T>(2);

				Childs[1] = value;
			}
		}

		public void Import(BinaryTreeNode<T> node)
		{
			Data = node.Data;
			Left = node.Left;
			Right = node.Right;
		}
		public bool IsEmpty()
		{
			return Childs == null;
		}
		public bool IsLeaf()
		{
			return Left == null && Right == null;
		}
		public void Clear()
		{
			Childs = null;
		}

	}
}
