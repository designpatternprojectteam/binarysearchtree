﻿namespace BinarySearchTree.Models
{
	public class Node<T>
	{
		private T _data;
		private NodeList<T> _childs;

		public Node()
		{
		}

		public Node(T data) : this(data, null)
		{
		}

		public Node(T data, NodeList<T> childs)
		{
			_data = data;
			_childs = childs;
		}

		public T Data
		{
			get
			{
				return _data;
			}
			set
			{
				_data = value;
			}
		}

		protected NodeList<T> Childs
		{
			get
			{
				return _childs;
			}
			set
			{
				_childs = value;
			}
		}
	}
}
