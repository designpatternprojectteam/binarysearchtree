﻿using BinarySearchTree.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Comparer
{
    public class IntegerComparer : ICComparer<IntegerNode>
    {
        public int Compare(IntegerNode val1, IntegerNode val2)
        {
            return val1.Value - val2.Value;
        }
    }
}
