﻿using BinarySearchTree.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Comparer
{
    public class NodeComparer : ICComparer<INodeValue>
    {
        public int Compare(INodeValue val1, INodeValue val2)
        {
            return val1.Compare(val2);
        }
    }
}
