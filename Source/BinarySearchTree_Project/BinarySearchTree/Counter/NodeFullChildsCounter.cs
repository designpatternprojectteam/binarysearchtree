﻿using BinarySearchTree.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Counter
{
    public class NodeFullChildsCounter<T> : Counter<T> where T : INodeValue
    {
        protected override bool MatchCondition(ValuableNode<T> valNode)
        {
            return valNode.IsFullChilds();
        }
    }
}
