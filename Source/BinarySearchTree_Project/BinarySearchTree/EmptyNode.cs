﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    public class EmptyNode<T>: Node<T>
    {
        public EmptyNode() 
        {
        }

        public EmptyNode(Node<T> parent)
        {
            base.Parent = Parent;
        }
        public override bool IsEmpty()
        {
            return true;
        }
    }
}
