﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Iterator
{
    public interface IIteratorBounding<T>
    {
        void Add(T data);
        IIterator<T> GetIterator();
    }
}
