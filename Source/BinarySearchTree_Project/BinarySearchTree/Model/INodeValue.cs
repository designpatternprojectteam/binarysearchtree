﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Model
{
    public interface INodeValue
    {
        Object GetValue();
        int Compare(INodeValue val2);
        string ConvertToString();
    }
}
