﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    public abstract class Node<T>
    {
        public Node<T> Parent { get; set; }
        public Node() { }
        public abstract bool IsEmpty();
        public bool IsParentEmpty()
        {
            return Parent == null || Parent.IsEmpty();
        }
        
    }
}
