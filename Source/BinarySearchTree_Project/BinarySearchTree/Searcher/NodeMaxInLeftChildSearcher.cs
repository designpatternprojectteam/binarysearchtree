﻿using BinarySearchTree;
using BinarySearchTree.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Searcher
{
    public class NodeMaxInLeftChildSearcher<T> : Searcher<T> where T : INodeValue
    {
        protected override Node<T> DoInSearch(ValuableNode<T> node)
        {
            if (node.Left.IsEmpty())
                return new EmptyNode<T>();
            return ((ValuableNode<T>)node.Left).GetMaxValue();
        }
    }
}
