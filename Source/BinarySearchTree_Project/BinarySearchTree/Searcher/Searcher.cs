﻿using BinarySearchTree.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree.Searcher
{
    public abstract class Searcher<T> where T : INodeValue
    {
        public Node<T> Search(Node<T> root)
        {
            if (root.IsEmpty())
                return new EmptyNode<T>();
            return DoInSearch((ValuableNode<T>)root);
        }
        protected abstract Node<T> DoInSearch(ValuableNode<T> node);
    }
}
